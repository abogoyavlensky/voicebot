#!/usr/bin/python
import aiml
import commands
 
k = aiml.Kernel()
 
# load the aiml file
k.learn("firsttry.aiml")
 
# set a constant
k.setBotPredicate("name", "Chatty")
 
while True:
    input = raw_input("> ")
    response = k.respond(input)
    # print out on the shell
    print response
    # and as speech
#    print commands.getoutput("/usr/bin/espeak -v en+f4 -p 99 -s 160 \"" + response + "\"")
    print commands.getoutput("""echo "{0}" | RHVoice -W Elena | aplay""".format(response))
#    print commands.getoutput("""echo "{0}" | RHVoice -W Aleksandr | aplay""".format(response))
